#+STARTUP: showall
#+TITLE: 100 days of emacs. Day 10
#+DATE: [24.01.2018]


* emacs daemon and client

  One of the the things bothering me about spacemacs is its load time. It is
  very high. One way to alleviate this problem is to load emacs in daemon mode
  and UI for emacs can loaded many times using emacsclient. 
  
** Starting emacs --deamon
   
   The first question I had to answer is when do I start the emacs daemon?
   
   There are may options, so let's go through them one by one ...

*** init / upstart etc.
    This not ideal as this starts up service/daemons per OS. I would rather
    the emacs deamon be started on a per user basis.

*** .profile, .bashrc etc.
    These are per login session. And a typical user can have many terminals open
    simultainously so this is not the right place.

*** Gnome (OR any Window Manger) start-up application
    Most Window Managers provide a way to specify start-up applications. ie
    applications that are loaded after login, when the window manager is also
    loading. This the correct time for me to load emacs's daemon.
    But I typically use XMonad and I keep switching around WMs so finally I
    decided to put this config in .xinitrc

** .xinitrc
   This gets called before the WMs is loaded so this is the perfect place to
   start-up the emacs daemon. In .xinitrc I added the following command

   #+BEGIN_SRC shell
     emacs --daemon
   #+END_SRC
   
   And that's it I could use emacsclient for the UI from that point on. Now 
   the only thing remaining was to add an alias for calling emacsclient. So in
   my .bash_alias (and shell config file will do) I added ...

   #+BEGIN_SRC shell
   alias ec='emacsclient -c'
   #+END_SRC

   And that's it. This seems to be reasonable setup, I'll work with this for a
   few weeks and see if it works for me in the long term.


* Emacs / Prelude

  Emacs prelude seems to be next popular of the "emacs starter kits" after 
  spacemacs. I thought to give prelude a shot given that I don't need vi 
  bindings at all. I did set Prelude up with out any problems by following the 
  directions given in the readme. It works quite well, a few of my initial 
  observation ...
  
*** Pros
    + loads considerable faster than spacemacs. 
    + Far more intutive user code loading scheme. ie. Any code in the `private`
      folder is loaded.
    + All the nice bits of spacemacs (helm, projectile, programming modes, etc.)
      are also present in Prelude and are just as easy to install/configure.

*** Cons 
    + Lacks the serious eye-candy of spacemacs

** Outcome
   I really like the eye-candy of spacemacs and I really don't mind the extra
   load time, especially since I have to do it only once as a daemon. So I'm 
   going to stick with spacemacs for now. I do plan to use Prelude or a similar
   starter kit as base for advanced set-up sometime later.

* Org-Mode Capture
  
  I did muck around the org-mode capture, but ran into many problems with it,
  first because some of spacemacs packages was messing this feature up. Then 
  when I started into the rabbit hole of upgrading my set-up, I had no time to 
  finish the org-mode / GTD setup. So I'm pushing that further down the line in
  my 100 days of emacs challenge.

