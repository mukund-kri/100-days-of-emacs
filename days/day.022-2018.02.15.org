#+STARTUP: showall
#+TITLE: 100 days of emacs. Day 22
#+DATE: [2018-02-15 Thu]


Back to work! Again after a few erratic days I'm fully back into this
challenge.

** Time-stamps
   This is my second go at time-stamps and I can appreciate it much more then
   when I last encountered it. 

*** What are org mode time-stamps?
    They are just date time strings in ISO 8601 formats. 
    
    Examples:
    - <2018-02-01 Thu 19:15>
    - <2018-02-15 Thu>

    What sets these strings apart in org-mode is ...
    - We can manipulate the dates with S-<up/down/left/right>
    - Ability to verify if the dates are valid 
    - Picked up with Agenda/Sparse Tree type operations
    - Highlighted differently in org-mode

*** Cheat sheet
    A few commands I use often in association with time-stamps

    | keybinding  | command                | description                               |
    |-------------+------------------------+-------------------------------------------|
    | C-c .       | org-time-stamp         | Pops up caleander, inserts date @ cursor  |
    | S-<left>    | org-timestamp-day-down | Decrement by one day                      |
    | S-<right>   | org-timestamp-day-up   | Increment by one day                      |
    | S-<up/down> |                        | Increment/Decrement the property on which |
    |             |                        | cursor is focused on                      |
    |-------------+------------------------+-------------------------------------------|

** Deadlines and Scheduling
   Another aspect of maintaining TODO's in org-files I had totally ignored 
   before are Deadlines and Scheduling. I am now starting to use these more
   often in my work, especially deadlines in conjunction with Sparse Trees.

*** Cheat sheet

    | keybinding | command             | description                                     |
    |------------+---------------------+-------------------------------------------------|
    | C-c / d    | org-check-deadlines | Task who's deadlines have passed or in the next |
    |            |                     | 14 days                                         |
    | C-c C-d    |                     | Set deadline for current task                   |
    | C-c C-s    |                     | Set schedule for current task                   |
    |------------+---------------------+-------------------------------------------------|

** #+Date: property in org-files and time-stamps
   On a side note, dates can be useful out side of TODO's. I just replace all my 
   snippets to use Inactive time-stamps for the Date property of the org files. 
   This ...
   1. Makes it easy to manipulate the data
   2. will be useful when I query the org files as a corpus

** Dealing with all those open buffers
   I was really bothered with all the open buffers in emacs but I ignored the problem
   till now. 
   Now all I do to close unused buffers is C-x C-b to open the buffer menu
   mark each of the buffers I want to close with d, and press x to kill all those 
   buffers.

** Tasks
*** DONE Describe time stamps
    CLOSED: [2018-02-15 Thu 09:31] DEADLINE: <2018-02-15 Thu>
*** DONE Change snippets to use time-stamps for the #+Date property
    CLOSED: [2018-02-15 Thu 09:50] DEADLINE: <2018-02-15 Thu>
*** DONE Deadlines and Scheduling
    CLOSED: [2018-02-15 Thu 09:45] DEADLINE: <2018-02-15 Thu>

