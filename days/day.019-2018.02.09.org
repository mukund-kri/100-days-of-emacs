#+STARTUP: showall
#+TITLE: 100 days of emacs. Day 19
#+DATE: [09.02.2018]

Yesterday was .. disappointing. I did get back into the challenge and wrote a
few lines of elisp code, but not much. Hoping today is better.

** Elisp

*** pcase 
    pcase is a "ML style pattern-language macro for ELisp". For a
    SML/Haskell/Scala programmer this is just what I need. So I dug in and
    DRY'ed all my org file generator functions.


