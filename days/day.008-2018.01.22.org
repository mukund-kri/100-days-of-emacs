#+STARTUP: showall
#+TITLE: 100 days of emacs. Day 8
#+DATE: [22.01.2018]

* org-mode zen

** The TODO Lists

*** Basics
    Nothing new to me here except crystallization of concepts and an in depth
    look into a world of customization.

**** cycling 
     - Any highlight can become a TODO by pressing S-left/S-right
     - pressing S-<left/right> again cycles through the TODO states

**** Priority 
     - S-<up/down>
     - 3 priorities by default - A, B, C

**** Customization : Todo State
     By default todo cycling has two states TODO and DONE. Where DONE is the end
     state and the completion time is recorded. This need be the only work flow
     and the states can be defined during emacs start-up with the following
     code.
     
     #+BEGIN_SRC elisp
     (setq org-todo-keywords
       '((sequence "TODO" "IN_PROGRESS" "|" "DONE" "ABANDONDED")))
     #+END_SRC

     Note: All the states after "|" are end states and the time is recorded when
     the todo is changed to this state.

**** Customization can also be per document
     The org todo states can also be customized on a per document basis with the
     TODO property

     #+BEGIN_SRC org
     #+TODO: TODO IN_PROGRESS | DONE ABANDONDED
     #+END_SRC

**** Todo types
     TODOs also can be configured to show the type of task instead of the state 
     of the task. In the following example a task can be of the types - Planning,
     Research, Coding and Documentation. The task has only one state that is 
     DONE.

     #+BEGIN_SRC org 
     #+TYP_TODO: Planning Research Coding Documentation | DONE     
     #+END_SRC

**** Many Todo state sets
     Not only can we customize the states and types of TODO. We can also have
     man state sets in a single file. The key to cycle between the state sets is 
     C-S-<left/right>


