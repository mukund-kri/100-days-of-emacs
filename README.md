# 100 days of emacs challenge

## Agenda

Inspired by the #100DaysOfCode challenges, in this version I plan to work on
improving my emacs skill which has been adequate but unchanged for a long time.

## Focus Topics

### Spacemacs
I really want to take a look at spacemacs and see if it can be my default 
setup.

### Improve my org-mode skills
I have been using org-mode as a todo tool (and not very effectively) and a 
pomodoro tracker. Over this challenge I want to use org-mode for the following
things too ...

1. As a documentation tool. Something like a super markdown.
2. To make presentations.
3. As a better planing tool with org-mode agenda.
4. Use org-pomodoro in org-mode.
5. Graphing my time usage with gnuplot.

### Improve my development work-flow

1. A concerted effort to write and manage snippets.
2. Use project new project template software like cookiecutter from inside
   emacs.
3. Look into the framework layers provided spacemacs to make developing in
   frameworks like django, react, expressJS etc. better.
4. Use magit for git.

### Elisp
1. Learn elisp as a language.
1. Write a lot of elisp functions to automate the boring stuff.
2. Maybe even write a spacemacs layer.

